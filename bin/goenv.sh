#!/usr/bin/env bash

# exit when any command fails
set -e

bin_dir=$(dirname "$(readlink -f "$0")")
image=$(basename "$bin_dir") 
project_dir=$(dirname "$(dirname "$bin_dir")") 
image_dir="$project_dir/images/$image"
current_image=registry.gitlab.com/dpom/devops-class-20200423/goenv:20200428

docker run -it \
       -e MY_USER=$USER \
       -e MY_UID="$(id -u)" \
       -e MY_GID="$(id -g)" \
       -e MY_PW="goenv" \
       -e DISPLAY=unix"$DISPLAY" \
       -e GDK_DPI_SCALE \
       -e GDK_SCALE \
       --net=host \
       --mount type=bind,source=/etc/localtime,target=/etc/localtime,readonly \
       --mount type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix \
       --mount type=bind,src="$(pwd)",dst="/home/$USER/workspace" \
        "$current_image" bash
 
