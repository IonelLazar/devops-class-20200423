package main

import "fmt"

func main() {
	cards := newDeck()

	hand, remainingCards := deal(cards, 5)

	// hand.print()
	// remainingCards.print()
	// fmt.Println(cards.toString())
	// cards.saveToFile("my_cards")
	// cards := newDeckFromFile("my_cards")
	cards.shufle()
	fmt.Println("Numarul de carti even: ", cards.countEven())
	fmt.Println("Numarul de hand even: ", hand.countEven())
	fmt.Println("Numarul de remainingCards even: ", remainingCards.countEven())

}
