package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

var (
	cardSuites     []string = []string{"Spades", "Hearts", "Diamonds", "Clubs"}
	cardOddValues  []string = []string{"Ace", "Three", "Five", "Seven"}
	cardEvenValues []string = []string{"Two", "Four", "Six", "Eight"}
)

// Create a new type 'deck'
// which is a slice of strings
type deck []string

func newDeck() deck {
	var cards deck

	for _, suite := range cardSuites {
		for _, value := range cardOddValues {
			cards = append(cards, value+" of "+suite)
		}
		for _, value := range cardEvenValues {
			cards = append(cards, value+" of "+suite)
		}

	}

	return cards
}

func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}

func newDeckFromFile(filename string) deck {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	s := strings.Split(string(bs), ",")
	return deck(s)
}

func (d deck) shufle() {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {
		newPosition := r.Intn(len(d) - 1)
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}

func isCardEven(card string) bool {
	for _, value := range cardEvenValues {
		if strings.HasPrefix(card, value) {
			return true
		}
	}
	return false
}

func (d deck) countEven() int {
	evenCount := 0
	for _, value := range d {
		if isCardEven(value) {
			evenCount++
		}
	}
	return evenCount
}
