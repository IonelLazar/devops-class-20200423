package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()

	if len(d) != 32 {
		t.Errorf("Expected deck length of 32 but got %v", len(d))
	}
}

func TestSaveToDeckAndNewFromFile(t *testing.T) {
	testFile := "_decktesting"

	os.Remove(testFile)

	deck := newDeck()
	deck.saveToFile(testFile)

	loadedDeck := newDeckFromFile(testFile)

	if len(loadedDeck) != 32 {
		t.Errorf("Expected 32 cards in deck, got %v", len(loadedDeck))
	}

	os.Remove(testFile)
}

func TestIsCardEven(t *testing.T) {

	if isCardEven("Ace of Harts") != false {
		t.Errorf("The card is odd but isCardEven returns %v", isCardEven("Ace of Harts"))
	}

	if isCardEven("Two of Spades") != true {
		t.Errorf("The card is even but isCardEven returns %v", isCardEven("Two of Spades"))
	}

	if isCardEven("cucu") != false {
		t.Errorf("This is not a card  but isCardEven returns %v", isCardEven("cucu"))
	}
}
