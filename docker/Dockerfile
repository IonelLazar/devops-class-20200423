FROM golang:1.14-buster

ENV DEBIAN_FRONTEND=noninteractive

# Install packages that is needed before visual studio code is installed
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils \
    libx11-xcb1 \
    curl \
    gpg \ 
    && rm -rf /var/lib/apt/lists/*

RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
RUN install -o root -g root -m 644 microsoft.gpg /usr/share/keyrings/microsoft-archive-keyring.gpg
RUN sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft-archive-keyring.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'


RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-transport-https \
    code \
    gosu \
    mc \
    libasound2 \
    libxss1 \
    libxtst6 \
    && rm -rf /var/lib/apt/lists/*

# Fix filewatchers is running out of handlers
RUN echo "fs.inotify.max_user_watches=524288" >> /etc/sysctl.conf

COPY scripts/*.sh /bin/

COPY extensions /extensions

RUN GO111MODULE=on go get golang.org/x/tools/gopls@latest

RUN go get github.com/mdempsky/gocode
RUN go get github.com/uudashr/gopkgs/v2/cmd/gopkgs
RUN go get github.com/ramya-rao-a/go-outline
RUN go get github.com/rogpeppe/godef
RUN go get github.com/sqs/goreturns
RUN go get golang.org/x/lint/golint
COPY bin/gocode-gomod /go/bin/

COPY config /config

ENTRYPOINT ["/bin/goenv.sh"]
