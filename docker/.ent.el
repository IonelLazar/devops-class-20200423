;;; .ent.el --- local ent config file -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; project settings
(setq ent-project-home (file-name-directory (if load-file-name load-file-name buffer-file-name)))
(setq ent-project-name "goenv")
(setq ent-clean-regexp "~$\\|\\.tex$")
(setq ent-project-orgfile "README.org")

(defun get-string-from-file (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

(setq image-name (get-string-from-file  "Version"))
 
(require 'ent)

(ent-tasks-init)

(task 'version  '() "display image name/version" '(lambda (&optional x) (concat "echo image: " image-name) ))


(task 'build  '() "build image" '(lambda (&optional x) (concat "docker build -t " image-name " .")))

(task 'lint  '() "lint dockerfile" '(lambda (&optional x) "docker run -it --rm -v \"$PWD/Dockerfile\":/Dockerfile:ro replicated/dockerfilelint:09a5034 /Dockerfile"))

(task 'deploy '() "deploy image to gitlab" '(lambda (&optional x) (concat "docker push " image-name)))


(provide '.ent)
;;; .ent.el ends here

;; Local Variables:
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
