#!/usr/bin/env sh

# exit when any command fails
set -e

/bin/create-user.sh "$MY_USER" "$MY_UID" "$MY_GID" "$MY_PW"

gosu "$MY_USER" mkdir -p "/home/$MY_USER/workspace"

for file in /extensions/*.vsix
do
    gosu "$MY_USER" code --install-extension  "${file}"
done
    
gosu "$MY_USER" mkdir -p "/home/$MY_USER/.config/Code/User/"
gosu "$MY_USER" cp /config/settings.json "/home/$MY_USER/.config/Code/User/"

exec gosu "$MY_USER" "$@" 

