#!/usr/bin/env bash

user=$1
uid=$2
gid=$3
pw=$4

mkdir -p /home/"$user"
echo "$user:x:$uid:$gid:$user,,,:/home/$user:/bin/bash" >>/etc/passwd
echo "$user:x:$uid:" >>/etc/group
mkdir -p /etc/sudoers.d
echo "$user ALL=(ALL) NOPASSWD: ALL" >/etc/sudoers.d/"$user"
chmod 0440 /etc/sudoers.d/"$user"
chown "$uid:$gid" -R /home/"$user"
echo -e "$pw\n$pw" | passwd "$user"
